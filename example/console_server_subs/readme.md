Example that uses server-side subscriptions.

You may enable `user_subscribe_to_personal` option in Centrifugo so that server will subscribe connection to a channel.
